import os

def checkUserPrivileges():
        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script.")

def humanReadableBytes(num):
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0