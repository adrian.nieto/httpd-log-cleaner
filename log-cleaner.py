#!/usr/bin/python

from util import utils
from logcleaner import LogCleaner as lc

path = '/var/log/httpd/'

if __name__ == '__main__':
    logc = lc.LogCleaner(path)

    utils.checkUserPrivileges()
    logc.processFileList(logc.searchLogs())