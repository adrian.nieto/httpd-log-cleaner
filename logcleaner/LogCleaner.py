import os
from util import utils

class LogCleaner():

    def __init__(self, path):
        self.path = path

    def searchLogs(self, pattern='.log-'):
        files = os.listdir(self.path)
        log_files = []
        for file in files:
            if file.find(pattern) != -1:
                log_files.append(file)
        return log_files

    def confirmDeletion(self, file_list, size):
        ans = raw_input(str(len(file_list))+" log files \""+utils.humanReadableBytes(size)+"\" are about to be deleted. Proceed? (yes/no): ")
        if ans == 'yes' or ans == 'y':
            for file in file_list:
                    os.remove(self.path+file)
            print "log files deleted !"
            exit()
        else:
            exit("Aborted")

    def processFileList(self, file_list):
        size = 0
        for file in file_list:
            print self.path+file
            size += os.stat(self.path+file).st_size
        self.confirmDeletion(file_list, size)